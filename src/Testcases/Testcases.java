package Testcases;

import ConcurTaskTree.Task;

import java.util.ArrayList;
import java.util.List;

//TODO: Iterable implementieren?
public class Testcases /*implements Iterable*/ {
    private final String name;
    private final List<List<Task>> testcases;

    public Testcases(Task task) {
        this.name = task.getName();
        this.testcases = new ArrayList<>();
        List<Task> initialTestcase = new ArrayList<>();
        initialTestcase.add(task);
        this.testcases.add(initialTestcase);
    }

    public Testcases(String name) {
        this.name = name;
        this.testcases = new ArrayList<>();
    }

    public String getName() { return this.name; }

    public int getNumberOfTestcases() { return this.testcases.size(); }

    public void addTestcase(List<Task> testcase) { this.testcases.add(testcase); }

    public List<Task> getTestcase(int i) { return this.testcases.get(i); }

    public void appendTestcases(List<List<Task>> newTestcases) {
        for (List<Task> newTestcase : newTestcases) {
            this.addTestcase(newTestcase);
        }
    }

    // Zum Testen
    public void print() {
        for (List<Task> testcase : this.testcases) {
            String printedString = "[";

            for (int i = 0; i < testcase.size(); i++) {
                Task task = testcase.get(i);
                if (i < testcase.size() - 1) {
                    printedString = printedString.concat(task.getName() + ", ");
                } else {
                    printedString = printedString.concat(task.getName() + "]");
                }
            }
            System.out.println(printedString);
        }
    }
}
