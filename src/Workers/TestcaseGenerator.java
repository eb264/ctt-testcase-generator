package Workers;

import ConcurTaskTree.CttOperator;
import ConcurTaskTree.Task;
import Helpers.ListUtil;
import Testcases.Testcases;

import java.util.ArrayList;
import java.util.List;

public class TestcaseGenerator {
    private static final int[] iterations = {1, 3, 10}; // Wie oft Iterationen wiederholt werden sollen (> 0)

    // Input: vereinfachtes CTT Modell als Java Objekt
    // Output: Liste von abstrakten Testfällen (Testfall = Liste von Basisaufgaben)
    public static Testcases generateTestcases(Task tree) throws CloneNotSupportedException {
        Testcases testcases = new Testcases(tree);
        boolean newTestcases = true;

        // Loop wird durchgeführt, bis keine neuen Testfälle mehr gefunden wurden
        while (newTestcases) {
            newTestcases = false;
            Testcases nextTestcases = new Testcases(testcases.getName());

            for (int i = 0; i < testcases.getNumberOfTestcases(); i++) {
                List<Task> currentTestcase = testcases.getTestcase(i);
                boolean isExpandable = checkExpandability(currentTestcase);

                if (isExpandable) {
                    List<List<Task>> expandedTestcases = expandTestcase(currentTestcase);
                    nextTestcases.appendTestcases(expandedTestcases);
                    newTestcases = true;
                } else {
                    nextTestcases.addTestcase(currentTestcase);
                }
            }
            testcases = nextTestcases;
        }
        return testcases;
    }

    // Überprüft, ob ein Testfall noch erweitert werden kann
    private static boolean checkExpandability(List<Task> testcase) {

        for (Task task : testcase) {
            if (task.getNumberOfSubtasks() > 0) {
                return true;
            }
        }
        return false;
    }

    // Erweitert einen Testfall
    private static List<List<Task>> expandTestcase(List<Task> testcase) {
        List<List<Task>> expandedTestcases = new ArrayList<>();
        expandedTestcases.add(new ArrayList<>());

        for (Task task : testcase) {
            List<List<Task>> subtaskCombinations = expandTask(task);

            expandedTestcases = ListUtil.getCartesian(expandedTestcases, subtaskCombinations);
        }
        return expandedTestcases;
    }

    // Erweitert einen Task
    private static List<List<Task>> expandTask(Task task) {
        List<List<Task>> expandedTask = new ArrayList<>();
        expandedTask.add(new ArrayList<>());

        if (task.getNumberOfSubtasks() > 0) {
            for (int i = 0; i < task.getNumberOfSubtasks(); i++) {
                Task currentTask = task.getSubtask(i);

                if (currentTask.getOperator() == CttOperator.ENABLING ||
                        currentTask.getOperator() == CttOperator.ENABLING_INFO || currentTask.getOperator() == null) {
                        // Falls die Aufgabe zusätzlich auch optional ist, bereits bekannte Folgen behalten
                        expandedTask = addTask(expandedTask, currentTask, currentTask.isOptional());
                } else if (currentTask.getOperator() == CttOperator.CHOICE) {
                    List<Task> choiceTasks = new ArrayList<>();
                    choiceTasks.add(currentTask);

                    // Alle Aufgaben sammeln, aus denen gewählt werden kann
                    for (i++;; i++) {
                        Task newChoiceTask = task.getSubtask(i);
                        choiceTasks.add(newChoiceTask);

                        if (newChoiceTask.getOperator() != CttOperator.CHOICE) {
                            break;
                        }
                    }

                    List<List<Task>> newExpandedTask = new ArrayList<>();

                    for (Task choiceTask : choiceTasks) {
                            newExpandedTask.addAll(addTask(expandedTask, choiceTask, false));
                    }

                    expandedTask = newExpandedTask;
                }
            }
        } else { // Falls die Aufgabe keine Unteraufgaben hat
            expandedTask = addTask(expandedTask, task, false);
        }
        return expandedTask;
    }

    private static List<List<Task>> addTask(List<List<Task>> testcases, Task newTask, boolean keepOldTestcases) {
        List<List<Task>> newTestcases = new ArrayList<>();
        int[] taskIterations = {1};

        // Standard: newTask wird nur einmal hinzugefügt; falls iterativ: so oft, wie in iterations angegeben
        if (newTask.isIterative()) {
            taskIterations = iterations;
        }

        for (int iterationCount : taskIterations) {
            for (List<Task> testcase : testcases) {
                List<Task> newTestcase = new ArrayList<>(testcase);

                for (int i = 0; i < iterationCount; i++) {
                    newTestcase.add(newTask);
                }
                newTestcases.add(newTestcase);
            }
        }

        if (keepOldTestcases) {
            // Clone wird erstellt, damit das Objekt testcases selbst nicht verändert wird
            List<List<Task>> testcasesClone = new ArrayList<>(testcases);
            testcasesClone.addAll(newTestcases);
            testcases = testcasesClone;
        } else {
            testcases = newTestcases;
        }

        return testcases;
    }
}
