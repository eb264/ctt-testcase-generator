package Workers;

import ConcurTaskTree.CttOperator;
import ConcurTaskTree.CttType;
import ConcurTaskTree.Task;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.Objects;
import java.util.Stack;

public class CttParser {

    // Input: CTT Modell im XML Format
    // Output: CTT Modell als Java Objekt
    public static Task parseConcurTaskTree(File xmlFile) throws ParserConfigurationException, IOException, SAXException{

        // Build DOM tree from XML File and get list of task elements
        DocumentBuilder DOMBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document tree = DOMBuilder.parse(xmlFile);
        NodeList elementList =  tree.getDocumentElement().getElementsByTagName("Task");

        // Initialize root task
        Element firstTask = (Element) elementList.item(0);
        Task rootTask = new Task(firstTask.getAttribute("Identifier"), getTypeEnum(firstTask.getAttribute("Category")),
                null, Boolean.parseBoolean(firstTask.getAttribute("Iterative")),
                Boolean.parseBoolean(firstTask.getAttribute("Optional")));

        // Initialize stack to help find parent task
        Stack<Task> taskPath = new Stack<>();
        taskPath.push(rootTask);

        // Construct java task model
        for (int i = 1; i < elementList.getLength(); i++) {
            // Initialize new subtask
            Element xmlTask = (Element) elementList.item(i);
            String operator = null;
            Element operatorElement = getDirectChild(xmlTask, "TemporalOperator");
            if (operatorElement != null) {
                operator = Objects.requireNonNull(operatorElement).getAttribute("name");
            }
            Task currentTask = new Task(xmlTask.getAttribute("Identifier"), getTypeEnum(xmlTask.getAttribute("Category")),
                    getOperatorEnum(operator), Boolean.parseBoolean(xmlTask.getAttribute("Iterative")),
                    Boolean.parseBoolean(xmlTask.getAttribute("Optional")));

            // Find parent and add subtask
            String parentName = Objects.requireNonNull(getDirectChild(xmlTask, "Parent")).getAttribute("name");
            while (!taskPath.empty()) {
                Task potentialParent = taskPath.peek();
                if (potentialParent.getName().equals(parentName)) {
                    potentialParent.addSubtask(currentTask);
                    taskPath.push(currentTask);
                    break;
                } else {
                    taskPath.pop();
                }
            }
        }

        return rootTask;
    }

    private static Element getDirectChild(Element parent, String name) {
        for(Node child = parent.getFirstChild(); child != null; child = child.getNextSibling()) {
            if(child instanceof Element && name.equals(child.getNodeName())) {
                return (Element) child;
            }
        }
        return null;
    }

    private static CttType getTypeEnum(String type) {
        if ("Interaction Task".equals(type)) {
            return CttType.INTERACTION_TASK;
        } else if ("User Task".equals(type)) {
            return CttType.USER_TASK;
        } else if ("Application Task".equals(type)) {
            return CttType.APPLICATION_TASK;
        } else {
            return CttType.ABSTRACT_TASK;
        }
    }

    private static CttOperator getOperatorEnum(String operator) {
        if ("SequentialEnabling".equals(operator)) {
            return CttOperator.ENABLING;
        } else if ("SequentialEnablingInfo".equals(operator)) {
            return CttOperator.ENABLING_INFO;
        } else if ("Choice".equals(operator)) {
            return CttOperator.CHOICE;
        } else if ("Interleaving".equals(operator)) {
            return CttOperator.CONCURRENCY;
        } else if ("Synchronization".equals(operator)) {
            return CttOperator.CONCURRENCY_INFO;
        } else if ("OrderIndependence".equals(operator)) {
            return CttOperator.ORDER_INDEPENDENCY;
        } else if ("Disabling".equals(operator)) {
            return CttOperator.DISABLING;
        } else if ("SuspendResume".equals(operator)){
            return CttOperator.SUSPEND_RESUME;
        } else {
            return null;
        }
    }
}
