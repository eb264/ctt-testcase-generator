package Workers;

import ConcurTaskTree.CttType;
import ConcurTaskTree.Task;
import Testcases.Testcases;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FeatureFileGenerator {
    // Standardanzahl von leeren Zeilen in der Tabelle unter Scenario Outline (EMPTY_ROWS > 0)
    private static final int EMPTY_ROWS = 2;
    // Ab wann im Testfall Application Tasks als Then-Startpunkt gelten (0 <= THEN_CUTOFF <= 1)
    private static final float THEN_CUTOFF = 0.7f;

    private final BufferedWriter output;

    private FeatureFileGenerator(String fileName) throws IOException{
        // BufferedWriter für effizienteres Schreiben
        FileWriter fileWriter = new FileWriter(fileName);
        this.output = new BufferedWriter(fileWriter);
    }

    public static void generateFeatureFile(Testcases testcases, String fileName) throws IOException{

        // fileName von "datei.xml" zu "datei.feature" ändern
        fileName = fileName.substring(0, fileName.lastIndexOf('.')) + ".feature";

        FeatureFileGenerator writer = new FeatureFileGenerator(fileName);

        writer.writeFeatureTitle(testcases.getName());

        List<Task> testcase;
        for (int i = 0; i < testcases.getNumberOfTestcases(); i++) {
            testcase = testcases.getTestcase(i);

            writer.writeScenario(testcase);
        }

        writer.close();
    }

    private void writeFeatureTitle(String titleName) throws IOException {
        output.write("Feature: " + titleName);
        output.newLine();
        output.newLine();
    }

    private void writeScenario(List<Task> testcase) throws IOException {
        List<String> parameters = getParameters(testcase);
        boolean containsParameters = parameters.size() > 0;

        writeScenarioTitle(containsParameters);
        writeSteps(testcase);
        if (containsParameters) {
            writeParameterTable(parameters);
        }
    }

    private List<String> getParameters(List<Task> testcase) {
        List<String> parameters = new ArrayList<>();

        for (Task task : testcase) {
            String taskName = task.getName();
            String[] words = taskName.split(" ");

            for (String word : words) {
                if (word.endsWith("?")) {
                    String parameter = word.substring(0, word.length() - 1);
                    parameters.add(parameter);
                }
            }
        }
        return parameters;
    }

    private void writeScenarioTitle(boolean outline) throws IOException {
        if (outline) {
            output.write("  Scenario Outline:");
        } else {
            output.write("  Scenario:");
        }
        output.newLine();
    }

    private void writeSteps(List<Task> testcase) throws IOException {
        int firstThenIndex = findFirstThen(testcase);
        boolean firstWhen = false;
        boolean firstThen = false;

        for (int i = 0; i < testcase.size(); i++) {
            Task task = testcase.get(i);

            if (i < firstThenIndex) {
                if (!firstWhen) {
                    firstWhen = true;
                    output.write("    When ");
                } else {
                    output.write("    And ");
                }
            } else {
                if (!firstThen) {
                    firstThen = true;
                    output.write("    Then ");
                } else {
                    output.write("    And ");
                }
            }

            String taskName = task.getName();

            // Falls es einen Platzhalter gibt, Wort für Wort überprüfen und richtig ausgeben
            if (taskName.contains("?")) {
                String[] words = taskName.split(" ");
                String word;

                for (int j = 0; j < words.length; j++) {
                    word = words[j];
                    if (word.endsWith("?")) {
                        word = "<" + word.substring(0, word.length() - 1) + "> ";
                    } else {
                        word += " ";
                    }

                    // letztes Wort kriegt kein Leerzeichen hinten
                    if (j == words.length - 1) {
                        word = word.trim();
                    }

                    output.write(word);
                }
            } else {
                output.write(taskName);
            }
            output.newLine();
        }
        output.newLine();
    }

    // Findet Index, ab dem die "Then" Schritte starten
    private int findFirstThen(List<Task> testcase) {
        int firstThenIndex = testcase.size() - 1;
        boolean foundLastGroup = false;

        for (int i = testcase.size() - 1; i >= testcase.size() * THEN_CUTOFF || foundLastGroup; i--) {
            if (foundLastGroup) {
                if (testcase.get(i).getType() == CttType.APPLICATION_TASK) {
                    firstThenIndex = i;
                } else {
                    break;
                }
            } else {
                if (testcase.get(i).getType() == CttType.APPLICATION_TASK) {
                    foundLastGroup = true;
                    firstThenIndex = i;
                }
            }
        }

        return firstThenIndex;
    }

    private void writeParameterTable(List<String> parameters) throws IOException {
        output.write("    Examples:");
        output.newLine();

        // Tabellenkopf
        output.write("      |");
        for (String parameter : parameters) {
            output.write(" " + parameter + " |");
        }
        output.newLine();

        // leere Zeilen
        for (int i = 0; i < EMPTY_ROWS; i++) {
            output.write("      |");
            for (String ignored : parameters) {
                output.write("  |");
            }
            output.newLine();
        }
        output.newLine();
    }

    private void close() throws IOException {
        output.close();
    }
}
