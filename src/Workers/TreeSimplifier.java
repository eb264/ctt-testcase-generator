package Workers;

import ConcurTaskTree.CttOperator;
import ConcurTaskTree.CttType;
import ConcurTaskTree.Task;

public class TreeSimplifier {

    // Vereinfacht das Aufgabenmodell mit Hilfe von Annahmen, die dies erleichtern
    public static void simplifyTaskTree(Task root) throws CloneNotSupportedException {
        Task childTask = null;

        for (int i = 0; i < root.getNumberOfSubtasks(); i++) {
            childTask = root.getSubtask(i);

            // User Tasks entfernen
            if (childTask.getType() == CttType.USER_TASK) {
                root.removeSubtask(i--); // Wenn i nicht verkleinert wird, wird eine Aufgabe übersprungen
            } else {
                // Operatoren anpassen
                CttOperator childOperator = childTask.getOperator();

                if (childOperator == CttOperator.CONCURRENCY || childOperator == CttOperator.CONCURRENCY_INFO ||
                        childOperator == CttOperator.ORDER_INDEPENDENCY) {
                    childTask.setOperator(CttOperator.ENABLING);
                } else if (childOperator == CttOperator.DISABLING) {
                    childTask.setOperator(CttOperator.ENABLING);
                    if (childTask.getNumberOfSubtasks() > 0) {
                        if (childTask.isIterative()) {
                            // Letzte Iteration "herausziehen" und als neuen Task einfügen, wird beim nächsten
                            // Schleifendurchlauf vereinfacht
                            Task childTaskClone = childTask.clone();

                            childTaskClone.setIterative(false);
                            childTaskClone.setOperator(CttOperator.DISABLING);
                            root.addSubtask(i + 1, childTaskClone);
                        } else {
                            // Die zweite Hälfte wird entfernt, da die unterbrechenden Subtasks in der Mitte eingefügt werden
                            int numberOfSubtasks = childTask.getNumberOfSubtasks();
                            for (int j = numberOfSubtasks / 2; j < numberOfSubtasks; j++) {
                                childTask.removeSubtask(j);
                            }

                            // Unterbrechende Aufgabe in unterbrochene Aufgabe verschieben.
                            // Bei Choice Operator den nächsten auch
                            Task interruptingTask;
                            do {
                                interruptingTask = root.getSubtask(i + 1);
                                root.removeSubtask(i + 1);
                                childTask.addSubtask(interruptingTask);
                            } while (interruptingTask.getOperator() == CttOperator.CHOICE);

                            interruptingTask.setOperator(null); // letzte Aufgabe darf keinen Operator haben
                        }
                    }
                } else if (childOperator == CttOperator.SUSPEND_RESUME) {
                    childTask.setOperator(CttOperator.ENABLING);
                    if (childTask.getNumberOfSubtasks() > 0) {
                        Task interruptingTask;
                        int insertIndex = childTask.getNumberOfSubtasks() / 2;
                        do {
                            interruptingTask = root.getSubtask(i + 1);
                            root.removeSubtask(i + 1);
                            childTask.addSubtask(insertIndex++, interruptingTask);
                        } while (interruptingTask.getOperator() == CttOperator.CHOICE);

                        interruptingTask.setOperator(CttOperator.ENABLING); // letzte Aufgabe darf keinen Operator haben
                    }
                }

                simplifyTaskTree(childTask);
            }

        }
        // Falls root Kinder hat, soll das letzte Kind keinen Operator haben
        if (childTask != null) {
            childTask.setOperator(null);
        }
    }

    // Gibt true zurück, wenn das Aufgabenmodell nicht vereinfacht ist
    // d.h. es enthält einen User Task oder Operatoren außer Choice und Enabling
    public static boolean needsSimplification(Task root) {

        if (root.getType() == CttType.USER_TASK || !(root.getOperator() == CttOperator.CHOICE ||
                root.getOperator() == CttOperator.ENABLING || root.getOperator() == CttOperator.ENABLING_INFO ||
                root.getOperator() == null)) {
            return true;
        }

        for (int i = 0; i < root.getNumberOfSubtasks(); i++) {

            Task child = root.getSubtask(i);

            if (needsSimplification(child)) {
                return true;
            }
        }

        return false;
    }
}
