package ConcurTaskTree;

import java.util.ArrayList;
import java.util.List;

public class Task implements Cloneable {

    private String name;
    private final CttType type;
    private CttOperator operator;
    private boolean iterative;
    private final boolean optional;
    private List<Task> subtasks;

    public Task(String name, CttType type, CttOperator operator, boolean iterative, boolean optional) {
        this.name = name;
        this.type = type;
        this.operator = operator;
        this.iterative = iterative;
        this.optional = optional;
        this.subtasks = new ArrayList<>();
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) { this.name = name; }

    public CttType getType() {
        return this.type;
    }

    public CttOperator getOperator() {
        return this.operator;
    }

    public void setOperator(CttOperator operator) { this.operator = operator; }

    public boolean isIterative() {
        return this.iterative;
    }

    public void setIterative(boolean iterative) { this.iterative = iterative; }

    public boolean isOptional() {
        return this.optional;
    }

    public void addSubtask(Task subtask) {
        this.subtasks.add(subtask);
    }

    public void addSubtask(int i, Task subtask) { this.subtasks.add(i, subtask); }

    public int getNumberOfSubtasks() { return this.subtasks.size(); }

    public Task getSubtask(int i) { return this.subtasks.get(i); }

    public void removeSubtask(int i) { this.subtasks.remove(i); }

    private void resetSubtasks() {
        this.subtasks = new ArrayList<>();
    }

    // Deep clone
    public Task clone() throws CloneNotSupportedException {
        Task clonedTask = (Task) super.clone();
        clonedTask.resetSubtasks();

        for (int i = 0; i < this.getNumberOfSubtasks(); i++) {
            clonedTask.addSubtask(this.getSubtask(i).clone());
        }

        return clonedTask;
    }

    // Zum Testen
    // Printed hierarchische Struktur mit Aufgabennamen, Typen und Operatoren
    public static void printTree(String indent, Task task) {
        CttOperator operator = task.getOperator();
        if (operator == null) {
            System.out.println(indent + task.getName() + " " + task.getType());
        } else {
            System.out.println(indent + task.getName() + " " + task.getType() + " " + task.getOperator());
        }

        for (int i = 0; i < task.subtasks.size(); i++) {
            printTree(indent + "    ", task.subtasks.get(i));
        }
    }
}
