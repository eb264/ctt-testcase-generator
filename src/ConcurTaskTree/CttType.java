package ConcurTaskTree;

public enum CttType {
    USER_TASK,
    INTERACTION_TASK,
    APPLICATION_TASK,
    ABSTRACT_TASK
}
