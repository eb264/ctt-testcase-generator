package ConcurTaskTree;

public enum CttOperator {
    ENABLING,
    ENABLING_INFO,
    CHOICE,
    CONCURRENCY,
    CONCURRENCY_INFO,
    ORDER_INDEPENDENCY,
    DISABLING,
    SUSPEND_RESUME
}
