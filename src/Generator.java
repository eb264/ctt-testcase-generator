import ConcurTaskTree.Task;
import Testcases.Testcases;
import Workers.CttParser;
import Workers.FeatureFileGenerator;
import Workers.TestcaseGenerator;
import Workers.TreeSimplifier;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.Locale;
import java.util.Scanner;

public class Generator {

    // args enthält ein Argument: der Pfad zur XML Datei
    public static void main(String[] args) {
        try {
            // Falls true, werden Aufgabenmodelle und Testfälle ausgegeben
            boolean showDebugInfo = args.length > 1 && "d".equals(args[1]);

            // Um Nutzereingaben auszuwerten
            Scanner in = new Scanner(System.in);

            File xmlFile = new File(args[0]);

            // Aus CTT-Modell im XML Format wird Java Objekt geschaffen
            System.out.println("XML-Datei wird verarbeitet...");
            Task tree = CttParser.parseConcurTaskTree(xmlFile);

            // DEBUG: Aufgabenmodell wird ausgegeben
            if (showDebugInfo) { Task.printTree("", tree); }

            // Prüfen, ob Aufgabenmodell Vereinfachung braucht
            System.out.println("Aufgabenmodell wird auf Vereinfachung geprüft...");
            if (TreeSimplifier.needsSimplification(tree)) {
                System.out.printf("%nDas Aufgabenmodell scheint nicht vereinfacht zu sein. Möchtest du es automatisch " +
                        "vereinfachen? WARNUNG: Dies wird nicht empfohlen. Vereinfache es selbst für das beste Ergebnis." +
                        " (j/n): ");
                String answer = in.nextLine().toLowerCase(Locale.GERMAN);

                // Falls Eingabe ungültig ist, frage erneut, bis es gültig ist
                while (!(answer.equals("j") || answer.equals("n"))) {
                    System.out.println("Keine gültige Eingabe.");
                    System.out.println("Möchtest du das Aufgabenmodell automatisch vereinfachen? WARNUNG: Dies wird " +
                            "nicht empfohlen. Vereinfache es selbst für das beste Ergebnis. (j/n): ");
                    answer = in.nextLine().toLowerCase(Locale.GERMAN);
                }

                // Auswertung der Eingabe
                // "j" = TaskTree automatisch vereinfachen; "n" = Programm beenden
                if (answer.equals("j")) {
                    System.out.println("Aufgabenmodell wird vereinfacht...");
                    TreeSimplifier.simplifyTaskTree(tree);
                } else {    //if (answer.equals("n"))
                    System.out.println("Anwendung wurde beendet. Starte es mit dem vereinfachten Aufgabenmodell neu.");
                    return;
                }

                // DEBUG: Automatisch vereinfachtes Aufgabenmodell wird ausgegeben
                if (showDebugInfo) { Task.printTree("", tree); }
            }


            // Testfälle generieren
            System.out.println("Abstrakte Testfälle werden aus vereinfachtem Aufgabenmodell generiert...");
            Testcases abstractTestcases = TestcaseGenerator.generateTestcases(tree);

            // DEBUG: Generierte Testfälle werden ausgegeben
            if (showDebugInfo) { abstractTestcases.print(); }

            // Testfälle zu Gherkin Feature Datei machen
            System.out.println("Feature Datei wird aus abstrakten Testfällen generiert...");
            FeatureFileGenerator.generateFeatureFile(abstractTestcases, xmlFile.getName());

            System.out.println("Die Feature Datei wurde erfolgreich erstellt. Programm wird beendet.");

        } catch (ArrayIndexOutOfBoundsException | ParserConfigurationException | IOException | SAXException |
                CloneNotSupportedException e) {
            e.printStackTrace();
            System.err.printf("%nEs ist ein Fehler aufgetreten. Anwendung wurde beendet.");
            if (e instanceof ArrayIndexOutOfBoundsException) {
                System.err.printf("%nStelle sicher, dass du wie folgt einen Pfad angegeben hast: " +
                        "'java Generator /pfad/zum/ctt/aufgabenmodell.xml' ");
            }
        }
    }
}
