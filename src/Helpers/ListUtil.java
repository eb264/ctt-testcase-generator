package Helpers;

import java.util.ArrayList;
import java.util.List;

public class ListUtil {

    public static <T> List<List<T>> getCartesian(List<List<T>> firstList, List<List<T>> secondList) {
        List<List<T>> cartesianList = new ArrayList<>();

        for (List<T> firstElement : firstList) {
            for (List<T> secondElement : secondList) {
                List<T> cartesianElement = new ArrayList<>(firstElement);
                cartesianElement.addAll(secondElement);
                cartesianList.add(cartesianElement);
            }
        }
        return cartesianList;
    }
}
