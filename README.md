# CTT Testcase Generator
Dieses Programm wurde im Rahmen einer Bachelorarbeit entwickelt. Das Ziel ist es, aus einem Aufgabenmodell in der
ConcurTaskTrees Notation abstrakte Testfälle im Gherkin Format herzuleiten. Das eingegebene CTT-Modell sollte 
idealerweise bereits nach den in der Bachelorarbeit beschriebenen Strategien vereinfacht sein. Falls nicht, gibt es die 
Option, dies automatisch machen zu lassen, wobei jedoch Annahmen getroffen werden müssen, die eventuell nicht zum besten
Ergebnis führen. Die Eingabe muss außerdem in dem XML-Format sein, wie es vom Programm CTTE[^1] ausgegeben wird. Das 
Ergebnis ist eine Gherkin Feature Datei, die mit Cucumber zur Ausführung von Tests benutzt werden kann.

[^1]: Vorgestellt in https://dx.doi.org/10.1109/tse.2002.1027801

## Hinweise
* Es muss Java installiert sein.
* Dieses Programm wurde mit Java 17 entwickelt und getestet. Die Kompatibilität mit anderen Java Versionen wurde nicht
  überprüft.
* Mehr Information zum Ansatz (z.B. die Vereinfachung) in der Bachelorarbeit.

## Nutzung 

1. Lade den Quellcode runter, z.B. mit `git clone`
2. Öffne das Terminal und navigiere mit `cd` zum "src"-Ordner
3. Kompiliere den Quellcode mit folgendem Befehl:
``` 
javac Generator.java -d ../out
```
4. Navigiere im Terminal zum neuen "out"-Ordner:
```
cd ../out
```
5. Führe das Programm mit dem Pfad zum Aufgabenmodell im XML-Format aus:
```
java Generator "/pfad/zur/XML/Datei/modell.xml"
```

6. Im "out"-Ordner befindet sich nun die Ausgabe als Datei mit der ".feature" Endung

## Debugging
Für mehr Information, die beim Debugging hilfreich sein könnten, das Programm wie folgt ausführen:
```
java Generator "/pfad/zur/XML/Datei/modell.xml" d
```
Folgendes wird damit bei der Ausführung zusätzlich ausgegeben:
* Das Aufgabenmodell nach dem Einlesen der XML-Datei
* Das Aufgabenmodell nach der automatischen Vereinfachung, falls diese stattgefunden hat
* Die generierten Testfälle (eine pro Zeile)


***